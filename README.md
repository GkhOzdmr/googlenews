# Welcome to Google News app

The Google News app is used to show you the most recent and popular news articles from various sources.

## Purpose

This application is created for displaying programming and Android skills of Gökhan Özdemir.

In this read me, you will find out about thought process of architectural choices and third party library implementations.

## Architectural choices

#### Lowest API level
The lowest api level is chosen to be 21 (Android Lollipop) due to removing the burden of creating two different versions of various Android native tools and leaving the old technology behind. Not only Android is improved after 21, but the manufactured devices are also equipped with better technology. Thus API >=21 is considered to be sweet spot of new Android applications.

#### Software Architecture
Mobile word is still hungry for technology, and when your work place is limited with a few inches, you have to think about every possibility.

The app designed to co-op with MVVM + Clean Architecture

This architecture gives you a well re-usability because of its nature. And when it comes to mobile, you would find yourself working with same screen/code peace all the time. Instead of creating same peace of work all the time, making it modular and able to be implemented at the many parts of the application is seems to be the smartest idea. Also, this makes it even easier to test different modules/peaces of the application and removes the burden of creating multiple interfaces that runs out of control.

Although many architectural choices can't find it's most power within this application, the many projects I worked at is responded well with this architectural choices. Thus I felt comfortable showing my knowledge about this architecture.


## Tools, Libraries, Frameworks

###### RxJava 2 
Although in this particular application, couldn't be able to find a good use case for its fully fledged power, RxJava2 is an excellent library for event based programming
and offers a real good API. 

###### Android Architecture Components
Android team has announced many good tools to make us Android developers' job easier.
In this case, I used a few of these components.

For Local storage, **ROOM** is a layer over SQLite and is quite performant. For more see: [ROOM](https://developer.android.com/topic/libraries/architecture/room)

**ViewModel** from Architecture Components help you with life cycles and in this case I considered using it for ease of tracking states. For more see: [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)

In conjunction with **ROOM** and **ViewModel**, **LiveData** find it self a use for creating observables. If wanted, this could be changed to RxJava2, but considering it's a part of Android Architecture Components, I decided to use it with other Architecture Components. For more see: [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)

Also, for the backbone of the current feature, **Paging** library used to retrieve the data gradually and restore the fetched data for offline use. For more see: [Paging](https://developer.android.com/topic/libraries/architecture/paging)

###### Dagger 2 
A Dependency Injection framework that's quite known by Android developers. I used to create dependency graph within this application. For more see: [Dagger 2](https://github.com/google/dagger)

###### Additional
For loading images and handling all other related businesses, [Glide](https://github.com/bumptech/glide) library used. 

For "loading animations" of Recycler View items, [Facebook's Shimmer](http://facebook.github.io/shimmer-android/) library put into use.

