package bonial.googlenews.android.features.topheadlines.domain.usecase

import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class CalculateWhenPostedTest {

    val calculateWhenPostedTest = CalculateWhenPosted()

    @Test
    fun `givenDate_ShouldBeConvertedToCalculatedTimeString`() {
        // GIVEN a date from api
        val DATE_FROM_API = "2019-09-02T09:29:00Z"
        val TODAY = "2019-09-02T12:29:00Z"
        println(TODAY)

        // WHEN date is parsed to yearCount, monthCount, dayCount, hourCount, minuteCount
        val testObserver = TestObserver<CalculatedTimes>()
        calculateWhenPostedTest.execute(CalculateWhenPosted.Params(DATE_FROM_API, TODAY.toString()))!!.subscribe(testObserver)

        // Making sure the observable completed successfully
        testObserver.awaitTerminalEvent()
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val calculatedTimes = testObserver.values()[0]

        println(calculatedTimes.yearCount)
        println(calculatedTimes.monthCount)
        println(calculatedTimes.dayCount)
        println(calculatedTimes.hourCount)
        println(calculatedTimes.minuteCount)

        // THEN returns count of fields (hour, minute, day, year, month)
        val EXPECTED = 3
        val ACTUAL = calculatedTimes.hourCount

        println(ACTUAL)
        assertThat(ACTUAL, `is`(equalTo(EXPECTED)))
    }

}