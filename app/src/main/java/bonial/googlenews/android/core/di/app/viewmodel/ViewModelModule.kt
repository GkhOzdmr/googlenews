package bonial.googlenews.android.core.di.app.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import bonial.googlenews.android.features.topheadlines.presentation.viewmodel.TopHeadlinesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(TopHeadlinesViewModel::class)
    abstract fun bindsTopHeadlinesViewModel(topHeadlinesViewModel: TopHeadlinesViewModel): ViewModel

}