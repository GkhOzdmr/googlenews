package bonial.googlenews.android.core.di.app

import bonial.googlenews.android.core.di.annotations.ActivityScope
import bonial.googlenews.android.core.di.app.viewmodel.ViewModelModule
import bonial.googlenews.android.core.di.data.NetworkModule
import bonial.googlenews.android.core.di.data.RepositoryModule
import bonial.googlenews.android.core.di.data.ServiceModule
import bonial.googlenews.android.core.di.domain.UseCaseModule
import bonial.googlenews.android.features.topheadlines.presentation.fragment.ArticleDetailFragment
import bonial.googlenews.android.features.topheadlines.presentation.fragment.TopHeadlinesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Dagger2 Module for providing Fragment instances
 */
@Module
abstract class FragmentBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            RepositoryModule::class,
            ServiceModule::class,
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindTopHeadlinesFragment(): TopHeadlinesFragment

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            RepositoryModule::class,
            ServiceModule::class,
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindArticleDetailFragment(): ArticleDetailFragment

}