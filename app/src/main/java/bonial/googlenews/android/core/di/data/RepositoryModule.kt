package bonial.googlenews.android.core.di.data

import bonial.googlenews.android.core.di.annotations.ActivityScope
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.data.repository.TopHeadlinesRepository
import dagger.Module
import dagger.Provides

/**
 * Dagger2 Module for providing repository interface implementor instances
 */
@Module
class RepositoryModule

