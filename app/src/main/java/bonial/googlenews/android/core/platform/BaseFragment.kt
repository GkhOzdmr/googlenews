package bonial.googlenews.android.core.platform

import android.os.Bundle
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import bonial.googlenews.android.R
import bonial.googlenews.android.features.MainActivity
import javax.inject.Inject

/**
 * Base Fragment class with common use functions and helper methods for handling views and back button events.
 */
abstract class BaseFragment : DaggerFragment() {

    abstract fun layoutId(): Int

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    internal val disposable = CompositeDisposable()
    internal var savedState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)

    /**
     * Handles fragment specific onBackPressed actions. If no action to be done
     * by fragment, method will return true and allow activity to call super.onBackPressed
     *
     * @return allow super.onBackPressed to be called. Return true to allow.
     */
    abstract fun onBackPressed(): Boolean

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

//    internal fun showProgress() = progressStatus(VISIBLE)
//
//    internal fun hideProgress() = progressStatus(GONE)

//    private fun progressStatus(viewStatus: Int) =
//        with(activity) { if (this is MainActivity) this.progress.visibility = viewStatus }

    /**
     * Disposes un-disposed subscriptions, should be called at onStop/onDestroy lifecycle state
     */
    internal fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.clear()
    }

    /**
     * Navigates to specified fragment
     *
     * @param fragment fragment to navigate to
     * @param destroyFragment keep the fragment state or not
     */
    fun navigateTo(fragment: Fragment, destroyFragment: Boolean) {
        fragmentManager!!.beginTransaction().apply {
            if (destroyFragment) replace(R.id.host_frame, fragment, fragment::class.simpleName)
            else add(R.id.host_frame, fragment, fragment::class.simpleName)
            addToBackStack(fragment::class.simpleName)
            commit()
        }
    }

    /**
     * Pops back to previous fragment
     */
    fun popBack() {
        fragmentManager!!.popBackStack()
    }

    /**
     * Displays Action bar at the top of the screen
     */
    internal fun showActionBar() {
        activity!!.findViewById<View>(R.id.appbar_layout).visibility = View.VISIBLE
    }

    /**
     * Hides the Action bar
     */
    internal fun hideActionBar() {
        activity!!.findViewById<View>(R.id.appbar_layout).visibility = View.GONE
    }

    internal fun hideKeyboard() {
        if (view != null) {
            val imm = context!!.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view!!.rootView.windowToken, 0)
            view!!.clearFocus()
        }
    }

//    internal fun showSnackbar(@StringRes message: Int) =
//        Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT).show()
}
