package bonial.googlenews.android.core.di.app

import bonial.googlenews.android.features.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import bonial.googlenews.android.core.di.annotations.ActivityScope
import bonial.googlenews.android.core.di.app.viewmodel.ViewModelModule
import bonial.googlenews.android.core.di.data.NetworkModule
import bonial.googlenews.android.core.di.data.RepositoryModule
import bonial.googlenews.android.core.di.data.ServiceModule
import bonial.googlenews.android.core.di.domain.UseCaseModule

/**
 * Dagger2 Activity injector
 *
 * Modules that activities use can be injected here
 */
@Module
abstract class ActivityBuilder {

    /**
     * Injects given modules to MainActivity
     */
    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            RepositoryModule::class,
            ServiceModule::class,
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindMainActivity(): MainActivity

}