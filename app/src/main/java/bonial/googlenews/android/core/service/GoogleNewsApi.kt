package bonial.googlenews.android.core.service

import bonial.googlenews.android.features.topheadlines.data.entity.TopHeadlinesEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Endpoints that belongs to Google's News API.
 *      Base URL: https://newsapi.org/v2/ (Documentation: https://newsapi.org/docs)
 *
 */
interface GoogleNewsApi {

    /**
     * Returns top headlines from Google's News API
     *
     *  Queries that is used:
     *      apiKey = Api key that is provided From Google
     *      language = Language in which the news will be returned as
     *      pageSize = amount of articles per page
     *      page = Page number
     *      q = Search query
     */
    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("apiKey") apiKey: String,
        @Query("language") language: String,
        @Query("pageSize") pageSize: String,
        @Query("page") page: String
    ): Observable<Response<TopHeadlinesEntity>>

}