package bonial.googlenews.android.core.di.data

import bonial.googlenews.android.BuildConfig
import bonial.googlenews.android.core.di.API_GOOGLE_NEWS
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import bonial.googlenews.android.core.di.annotations.ActivityScope
import java.util.concurrent.TimeUnit
import javax.inject.Named


/**
 * Dagger2 Module for providing Network tools instances
 */
@Module
class NetworkModule {

    /**
     * Provides Retrofit instance for API_GOOGLE_NEWS
     *
     * Base URL: [BuildConfig.GOOGLE_NEWS_BASE_URL]
     *
     * @param gson GSon serializer
     * @param okHttpClient OkHttpClient of OkHttp3
     * @return Instance of Retrofit with @Named(API_GOOGLE_NEWS) annotation
     */
    @ActivityScope
    @Provides
    @Named(API_GOOGLE_NEWS)
    fun provideRetrofitForGoogleNewsApi(gson: Gson, okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.GOOGLE_NEWS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    /**
     *
     * Provides GSon instance
     *
     * @return gson instance
     */
    @ActivityScope
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().setLenient().create()
    }

    /**
     *
     * Provides OkHttpClient with logging interceptor
     * You can check api activity from logs with "OkHttp" tag
     *
     * Timeouts for api connections can be set here(Use java.util.concurrent.TimeUnit version of TimeUnit)
     *
     * @return OkHttpClient instance
     */
    @ActivityScope
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val loginInterceptor = HttpLoggingInterceptor()
        loginInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .connectTimeout(30000, TimeUnit.MILLISECONDS)
            .readTimeout(30000, TimeUnit.MILLISECONDS)
            .writeTimeout(30000, TimeUnit.MILLISECONDS)
            .addInterceptor(loginInterceptor)
            .build()
    }

}