package bonial.googlenews.android.core.di.data

import android.content.Context
import androidx.room.Room
import bonial.googlenews.android.core.di.API_GOOGLE_NEWS
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import bonial.googlenews.android.core.di.SERVICE_GOOGLE_NEWS
import bonial.googlenews.android.core.di.annotations.ActivityScope
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import javax.inject.Named

/**
 * Dagger 2 Module for providing endpoint Services
 */
@Module
class ServiceModule {

    /**
     * Provides service to be used for communicating with swapi.co endpoints
     */
    @ActivityScope
    @Provides
    fun provideNewsApiService(@Named(API_GOOGLE_NEWS) retrofit: Retrofit): GoogleNewsApi {
        return retrofit.create(GoogleNewsApi::class.java)
    }

    /**
     * Provides ROOM Database that consists of tables of DTO's that is used in Google News API
     */
    @ActivityScope
    @Provides
    fun provideNewsApiDatabase(context: Context): GoogleNewsDatabase {
        return Room.databaseBuilder(context, GoogleNewsDatabase::class.java, "google_news.db").build()
    }

}