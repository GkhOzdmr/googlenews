package bonial.googlenews.android.core.service

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesDAO
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.data.entity.SourceEntityTypeConverter

/**
 * ROOM Database implementation class
 *
 * The instance of database is supplied via Dagger 2. All the Entities and other components
 * of ROOM should be handled here.
 */
@Database(entities = [
    ArticlesEntity::class], version = 1)
@TypeConverters(SourceEntityTypeConverter::class)
abstract class GoogleNewsDatabase : RoomDatabase() {
    abstract fun articlesDAO(): ArticlesDAO
}