package bonial.googlenews.android.core.di.app

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import bonial.googlenews.android.core.platform.GoogleNewsApp

/**
 * Dagger2 Component for injecting modules to application
 */
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    AppModule::class
])
interface AppComponent : AndroidInjector<GoogleNewsApp> {

    /**
     * Injects dependencies with declared modules
     */
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: GoogleNewsApp) : Builder
        fun build() : AppComponent
    }

}