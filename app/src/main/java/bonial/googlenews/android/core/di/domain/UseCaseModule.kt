package bonial.googlenews.android.core.di.domain

import dagger.Module
import dagger.Provides
import bonial.googlenews.android.core.di.annotations.ActivityScope
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.domain.usecase.CalculateWhenPosted
import bonial.googlenews.android.features.topheadlines.domain.usecase.GetArticles

/**
 * Dagger2 Module for providing UseCases
 */
@Module
class UseCaseModule {

    @ActivityScope
    @Provides
    fun provideGetArticlesUseCase(
        googleNewsApi: GoogleNewsApi,
        googleNewsDatabase: GoogleNewsDatabase
    ) = GetArticles(googleNewsApi, googleNewsDatabase)

    @ActivityScope
    @Provides
    fun provideCalculateWhenPosted() = CalculateWhenPosted()
}