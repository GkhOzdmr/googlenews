package bonial.googlenews.android.core.functional

/**
 * Sealed class to determine the state of network.
 * Use to notify observers for network actions such as loading, error etc.
 */
sealed class NetworkState {

    //TODO Document
    object Loading : NetworkState()
    data class Error(val errorMessage: String = "") : NetworkState()
    object Finished : NetworkState()
    object TimeOut : NetworkState()

}