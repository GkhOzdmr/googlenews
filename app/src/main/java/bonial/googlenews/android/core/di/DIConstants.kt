package bonial.googlenews.android.core.di

/**
 * Constant values to be used in Dagger2 @Named annotations
 */

const val SERVICE_GOOGLE_NEWS = "SERVICE_GOOGLE_NEWS"
const val API_GOOGLE_NEWS = "SERVICE_GOOGLE_NEWS"