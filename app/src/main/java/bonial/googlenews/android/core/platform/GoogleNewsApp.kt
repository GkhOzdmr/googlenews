package bonial.googlenews.android.core.platform

import android.content.Context
import androidx.multidex.MultiDex
import bonial.googlenews.android.BuildConfig
import bonial.googlenews.android.core.di.app.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

class GoogleNewsApp: DaggerApplication() {

    /**
     * Dagger2 injector plantation for auto injecting to activities and fragments
     *
     * @see [ActivityBuilder]
     * @see [FragmentBuilder]
     */
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        plantTimber()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        plantMultiDex()
    }

    /**
     * Timber plantation. On release version, Timber logs won't be compiled.
     */
    private fun plantTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    /**
     * Installing MultiDex
     */
    private fun plantMultiDex() {
        MultiDex.install(this)
    }

}