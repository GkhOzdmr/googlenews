package bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines

import android.content.Context
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import bonial.googlenews.android.R
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.domain.model.ArticleModel
import bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.viewholder.ArticlesNetworkStateViewHolder
import bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.viewholder.ArticlesViewHolder
import com.bumptech.glide.RequestManager

/**
 * RecyclerView Adapter implementation to show top headlines that's
 * fetched from Google News API
 *
 * @see bonial.googlenews.android.core.service.GoogleNewsApi
 *
 * @property context Context in which this adapter will be instantiated at
 * @property notifier A simple interface to notify about adapter actions. Should be implemented at instantiation site
 */
class TopHeadlinesAdapter(
    val context: Context,
    val glide: RequestManager,
    val notifier: Notifier
) : PagedListAdapter<ArticlesEntity, RecyclerView.ViewHolder>(ARTICLES_COMPARATOR) {

    private var networkState: NetworkState? = null

    /**
     * A simple interface to notify about adapter actions. Should be implemented at instantiation site
     */
    interface Notifier {
        /**
         * Should be invoked when user clicks on the particular article to view more details about it.
         *
         * @param articleDetails Particular DTO that belongs to article
         */
        fun onHeadlineClick(
            articleDetails: ArticleModel?,
            imageView: ImageView?
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_top_headlines-> {
                (holder as ArticlesViewHolder).bind(getItem(position))
            }

            R.layout.item_top_headlines_shimmer -> {
                (holder as ArticlesNetworkStateViewHolder).bind(networkState)
            }
        }
    }

    /**
     * Sets the network state observed from data layer
     *
     * If the data is loaded, notifies recycler view for view holder changes.
     */
    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                for (i in 0..21) {
                    notifyItemRemoved(super.getItemCount() - i)
                }
            } else {
                for (i in 0..21) {
                    notifyItemInserted(super.getItemCount() - i)
                }
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            for (i in 0..21) {
                notifyItemChanged(super.getItemCount() - i)
            }
        }
    }

    /**
     * A boolean indicating there will be new item/items loaded
     */
    private fun hasExtraRow() = networkState != null && networkState !is NetworkState.Finished && networkState !is NetworkState.Error

    /**
     * If there will be request for new data, there will be 21 new placeholder item for showing
     * loading indication.
     */
    override fun getItemCount(): Int = super.getItemCount() + if (hasExtraRow()) 21 else 0

    /**
     * If there will be new rows coming, view holder will be for showing loading state, else
     * usual item for displaying data.
     */
    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position <= itemCount - 1) {
            R.layout.item_top_headlines_shimmer
        } else {
            R.layout.item_top_headlines
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_top_headlines -> { ArticlesViewHolder.create(parent, notifier, glide) }
            R.layout.item_top_headlines_shimmer -> { ArticlesNetworkStateViewHolder.create(parent) }
            else -> { ArticlesNetworkStateViewHolder.create(parent) }
        }
    }

    companion object {
        val ARTICLES_COMPARATOR = object : DiffUtil.ItemCallback<ArticlesEntity>() {
            override fun areItemsTheSame(
                oldItem: ArticlesEntity,
                newItem: ArticlesEntity
            ): Boolean = oldItem == newItem

            override fun areContentsTheSame(
                oldItem: ArticlesEntity,
                newItem: ArticlesEntity
            ): Boolean = oldItem.title == newItem.title

        }
    }
}