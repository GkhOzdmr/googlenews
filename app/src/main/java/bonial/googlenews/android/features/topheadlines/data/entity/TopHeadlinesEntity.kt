package bonial.googlenews.android.features.topheadlines.data.entity

import bonial.googlenews.android.features.topheadlines.domain.model.TopHeadlinesModel
import com.google.gson.annotations.SerializedName

/**
 * This is a general DTO class of Google News Top Headlines request result.
 *
 * Contains status and amount of result combined with list of articles.
 * Should be only used to retrieve data from Google News API
 *
 * On the presentation layer, model classes should be used.
 */
data class TopHeadlinesEntity(
    @SerializedName("status") val status : String?,
    @SerializedName("totalResults") val totalResults : Int?,
    @SerializedName("articles") val articles : List<ArticlesEntity>?
){

    /**
     * Converts this Entity DTO to model DTO to be used in domain/presentation layer.
     */
    fun toModel(): TopHeadlinesModel {
        return TopHeadlinesModel(
            status,
            totalResults,
            articles?.map {  articlesEntity -> articlesEntity.toModel() }
        )
    }
}