package bonial.googlenews.android.features.topheadlines.presentation.fragment

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bonial.googlenews.android.R
import bonial.googlenews.android.core.extension.viewModel
import bonial.googlenews.android.core.platform.BaseFragment
import bonial.googlenews.android.core.platform.GlideApp
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.domain.model.ArticleModel
import bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.TopHeadlinesAdapter
import bonial.googlenews.android.features.topheadlines.presentation.viewmodel.TopHeadlinesViewModel
import kotlinx.android.synthetic.main.fragment_top_headlines.*

/**
 * A simple [Fragment] subclass.
 *
 * Used to display news articles that comes from Google News API
 *
 * Up on clicking articles, [ArticleDetailFragment] fragment will open displaying
 * article details and linking to source of article.
 */
class TopHeadlinesFragment : BaseFragment(),
    TopHeadlinesAdapter.Notifier {

    override fun layoutId(): Int = R.layout.fragment_top_headlines

    override fun onBackPressed(): Boolean = true

    lateinit var topHeadlinesViewModel: TopHeadlinesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    /**
     * Initializes view components
     */
    private fun initView() {
        initRecyclerView()
    }

    /**
     * Observes ViewModel observables
     */
    private fun initViewModel() {
        topHeadlinesViewModel = viewModel(viewModelFactory) { }
    }

    /**
     * Initializes RecyclerView and it's components
     */
    private fun initRecyclerView() {
        val glide = GlideApp.with(context!!)
        val headlinesAdapter =
            TopHeadlinesAdapter(context!!, glide, this@TopHeadlinesFragment)

        topHeadlinesViewModel.articlesObserver.observe(this@TopHeadlinesFragment, Observer {
            headlinesAdapter.submitList(it)

        })

        topHeadlinesViewModel.networkState.observe(this@TopHeadlinesFragment, Observer {
            headlinesAdapter.setNetworkState(it)
        })

        rc_top_headlines.apply {
            adapter = headlinesAdapter
            layoutManager = GridLayoutManager(context, 6, RecyclerView.VERTICAL, false).apply {
                var isPortrait =
                    resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return when {
                            position == 0 -> {
                                6
                            }
                            position % 7 == 0 -> {
                                6
                            }
                            else -> {
                                if (isPortrait) 3 else 2
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * This functions is overridden from TopHeadlinesAdapter
     *
     * When users click on a specific article, goes to a separate fragment
     * that displays more detail about article
     *
     * @param articleDetails Specific [ArticleModel] to send to fragment
     */
    override fun onHeadlineClick(
        articleDetails: ArticleModel?,
        imageView: ImageView?
    ) {
        if(articleDetails == null) return
        val fragment = ArticleDetailFragment.newInstance(articleDetails, ViewCompat.getTransitionName(imageView!!)!!)
        openArticleDetail(fragment, imageView)
    }

    /**
     * Opens article detail fragment with the given DTO. Plays animation while transitioning.
     */
    private fun openArticleDetail(fragment: Fragment, imageView: ImageView?) {
        fragmentManager!!.beginTransaction().apply {
            addSharedElement(imageView!!, ViewCompat.getTransitionName(imageView)!!)
            add(R.id.host_frame, fragment, fragment::class.simpleName)
            addToBackStack(fragment::class.simpleName)
            commit()
        }

    }

    companion object {

        /**
         * Returns instance of this fragment class
         *
         */
        @JvmStatic
        fun newInstance(): TopHeadlinesFragment {
            return TopHeadlinesFragment()
        }
    }
}
