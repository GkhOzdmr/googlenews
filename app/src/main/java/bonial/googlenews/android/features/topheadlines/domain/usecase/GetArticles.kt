package bonial.googlenews.android.features.topheadlines.domain.usecase

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import bonial.googlenews.android.core.di.SERVICE_GOOGLE_NEWS
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.data.repository.TopHeadlinesRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

/**
 * This use case returns [ArticlesEntity] PagedList
 *
 * This particular UseCase doesn't contain RxJava observables since retrieving and restoring data is
 * handled via Paging library and for convenient of the architectural choice, live data used for observable pattern.
 *
 * RxJava still can be used as observable pattern choice.
 */
class GetArticles @Inject constructor(
    val googleNewsApi: GoogleNewsApi,
    val googleNewsDatabase: GoogleNewsDatabase
) {

    private val disposable = CompositeDisposable()
    val networkState: MutableLiveData<NetworkState>
    val articlesObservable: MediatorLiveData<PagedList<ArticlesEntity>>

    init {
        val repository = TopHeadlinesRepository(googleNewsApi, googleNewsDatabase, disposable)
        networkState = repository.networkState
        articlesObservable = repository.liveDataMerger
    }

    fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

}