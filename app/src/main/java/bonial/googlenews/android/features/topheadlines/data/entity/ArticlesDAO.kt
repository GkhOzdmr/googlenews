package bonial.googlenews.android.features.topheadlines.data.entity

import androidx.paging.DataSource
import androidx.room.*

@Dao
interface ArticlesDAO {

    @Query("SELECT * FROM articles")
    fun getArticles(): DataSource.Factory<Int, ArticlesEntity>

    @Insert
    fun insertArticle(article: ArticlesEntity)

    @Insert
    fun insertAll(articles: List<ArticlesEntity>)

    @Delete
    fun delete(article: ArticlesEntity)

    @Update
    fun update(article: ArticlesEntity)


}