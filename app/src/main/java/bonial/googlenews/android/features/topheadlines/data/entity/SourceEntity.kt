package bonial.googlenews.android.features.topheadlines.data.entity

import androidx.room.ColumnInfo
import bonial.googlenews.android.features.topheadlines.domain.model.SourceModel
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * DTO class to be used at "data layer"
 *
 * Returns inside of [TopHeadlinesEntity]
 */
data class SourceEntity(
    @SerializedName("id")
    @ColumnInfo(name = "source_id")
    val id : String?,

    @SerializedName("name")
    @ColumnInfo(name = "source_name")
    val name : String?

){


    /**
     * Converts this Entity DTO to model DTO to be used in domain/presentation layer.
     */
    fun toModel(): SourceModel {
        return SourceModel(
            id,
            name
        )
    }
}