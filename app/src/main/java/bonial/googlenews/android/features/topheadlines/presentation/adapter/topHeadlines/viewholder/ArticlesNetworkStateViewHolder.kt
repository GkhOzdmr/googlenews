package bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import bonial.googlenews.android.R
import bonial.googlenews.android.core.functional.NetworkState
import kotlinx.android.synthetic.main.item_top_headlines_shimmer.view.*

/**
 * View Holder class for displaying
 * loading, error states for [bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.TopHeadlinesAdapter]
 *
 */
class ArticlesNetworkStateViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    val shimmer_item_top_headlines = view.shimmer_item_top_headlines

    fun bind(networkState: NetworkState?) {
        if (networkState == null) return
        when (networkState) {
            is NetworkState.Loading -> {
                shimmer_item_top_headlines.startShimmer()
            }

            else -> {
                shimmer_item_top_headlines.stopShimmer()
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): ArticlesNetworkStateViewHolder {
            return ArticlesNetworkStateViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_top_headlines_shimmer,
                    parent,
                    false
                )
            )
        }
    }
}