package bonial.googlenews.android.features.topheadlines.data.entity

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import bonial.googlenews.android.features.topheadlines.domain.model.ArticleModel
import com.google.gson.annotations.SerializedName

/**
 * DTO class to be used at "data layer"
 *
 * Returns inside of [TopHeadlinesEntity]
 */
@Entity(tableName = "articles")
data class ArticlesEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Long,

    @SerializedName("source")
    @TypeConverters(SourceEntityTypeConverter::class)
    @ColumnInfo(name = "article_source")
    @Nullable
    val source: SourceEntity?,

    @SerializedName("author")
    @ColumnInfo(name = "article_auther")
    @Nullable
    val author: String?,

    @SerializedName("title")
    @ColumnInfo(name = "article_title")
    @Nullable
    val title: String?,

    @SerializedName("description")
    @ColumnInfo(name = "article_description")
    @Nullable
    val description: String?,

    @SerializedName("url")
    @ColumnInfo(name = "article_url")
    @Nullable
    val url: String?,

    @SerializedName("urlToImage")
    @ColumnInfo(name = "article_urlToImage")
    @Nullable
    val urlToImage: String?,

    @SerializedName("publishedAt")
    @ColumnInfo(name = "article_publishedAt")
    @Nullable
    val publishedAt: String?,

    @SerializedName("content")
    @ColumnInfo(name = "article_content")
    @Nullable
    val content: String?
) {

    /**
     * Converts this Entity DTO to model DTO to be used in domain/presentation layer.
     */
    fun toModel(): ArticleModel {
        return ArticleModel(
            source?.toModel(),
            author,
            title,
            description,
            url,
            urlToImage,
            publishedAt,
            content
        )
    }
}