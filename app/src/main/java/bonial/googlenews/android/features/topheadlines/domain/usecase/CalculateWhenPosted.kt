package bonial.googlenews.android.features.topheadlines.domain.usecase

import bonial.googlenews.android.core.interactor.UseCase
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalArgumentException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

//TODO Document
class CalculateWhenPosted : UseCase<CalculatedTimes, CalculateWhenPosted.Params>() {

    override fun buildUseCaseObservable(params: Params): Observable<CalculatedTimes> {
        return Observable.just(calculatePostedDate(params.dateWhenPosted, params.today))
            .subscribeOn(Schedulers.io())
    }

    private fun getFormattedDate(dateFormatToConvert: String): Date {
        val oldDatePatterns = listOf(
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"),
            SimpleDateFormat("E, MMM dd HH:mm:ss z yyyy")
        )

        var oldDate = Date()

        patternLoop@for (pattern in oldDatePatterns) {
            try {
                oldDate = pattern.parse(dateFormatToConvert)
                break@patternLoop
            } catch (parseException: ParseException) {
                //If the date format is not correct, continue.
            }
        }

        return oldDate
    }

    private fun calculatePostedDate(publishDate: String, today: String): CalculatedTimes {
        val formattedPublishDate = getFormattedDate(publishDate)

        val formatTodayDateTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(today)

        val calendar = Calendar.getInstance()
        calendar.time = formatTodayDateTime

        var year = calendar.get(Calendar.YEAR)
        var month = calendar.get(Calendar.MONTH)
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        var hour = calendar.get(Calendar.HOUR_OF_DAY)
        var minute = calendar.get(Calendar.MINUTE)

        calendar.time = formattedPublishDate

        year -= calendar.get(Calendar.YEAR)
        month -= calendar.get(Calendar.MONTH)
        day -= calendar.get(Calendar.DAY_OF_MONTH)
        hour -= calendar.get(Calendar.HOUR_OF_DAY)
        minute -= calendar.get(Calendar.MINUTE)

        return CalculatedTimes(
            year,
            month,
            day,
            hour,
            minute
        )
    }

    data class Params(val dateWhenPosted: String, val today: String)
}
