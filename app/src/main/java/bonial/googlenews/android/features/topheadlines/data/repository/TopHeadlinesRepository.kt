package bonial.googlenews.android.features.topheadlines.data.repository

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

/**
 * Repository to handle different data sources depending on state and return data to domain layer.
 *
 * Uses remote and local sources with the help of Paging Library
 *
 * Please refer to Paging Library for detailed information.(link:https://developer.android.com/topic/libraries/architecture/paging)
*/
class TopHeadlinesRepository constructor(
    val googleNewsApi: GoogleNewsApi,
    val googleNewsDatabase: GoogleNewsDatabase,
    val disposable: CompositeDisposable
) {

    var networkState: MutableLiveData<NetworkState>
    val liveDataMerger: MediatorLiveData<PagedList<ArticlesEntity>> = MediatorLiveData()

    init {
        val dataSourceFactory = TopHeadlinesDataSourceFactory(googleNewsApi, googleNewsDatabase, disposable)
        networkState = dataSourceFactory.networkState

        val pagedListBuilder = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(21)
            .setPageSize(21)
            .build()

        val executor = Executors.newFixedThreadPool(3)

        val livePagedListBuilder = LivePagedListBuilder(dataSourceFactory, pagedListBuilder)

        val boundaryCallback = object : PagedList.BoundaryCallback<ArticlesEntity>() {
            override fun onZeroItemsLoaded() {
                super.onZeroItemsLoaded()

                val dbPagedListConfig = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setInitialLoadSizeHint(Integer.MAX_VALUE)
                    .setPageSize(Integer.MAX_VALUE)
                    .build()

                val dbLivePagedBuilder =
                    LivePagedListBuilder(
                        googleNewsDatabase.articlesDAO().getArticles(),
                        dbPagedListConfig)
                        .setFetchExecutor(executor)
                        .build()

                liveDataMerger.addSource(dbLivePagedBuilder) { value ->
                    liveDataMerger.value = value
                    liveDataMerger.removeSource(dbLivePagedBuilder)
                }
            }
        }

        val articlesPagedList = livePagedListBuilder
            .setFetchExecutor(executor)
            .setBoundaryCallback(boundaryCallback)
            .build()

        liveDataMerger.addSource(articlesPagedList) { value ->
            liveDataMerger.setValue(value)
        }

        saveToLocal(dataSourceFactory)
    }

    /**
     * Saves data that's fetched from remote to local data store
     */
    private fun saveToLocal(dataSourceFactory: TopHeadlinesDataSourceFactory) {
        disposable.add(
            dataSourceFactory.articles
                .observeOn(Schedulers.io())
                .subscribe { articleEntity ->
                    googleNewsDatabase.articlesDAO().insertArticle(articleEntity)
                }
        )

    }

}