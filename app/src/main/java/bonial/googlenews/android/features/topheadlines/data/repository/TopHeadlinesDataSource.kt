package bonial.googlenews.android.features.topheadlines.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import bonial.googlenews.android.BuildConfig
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.ReplaySubject
import javax.inject.Inject

/**
 * Data source for Android Paging Library.
 * Please refer to Paging Library for detailed information.(link:https://developer.android.com/topic/libraries/architecture/paging)
 */
class TopHeadlinesDataSource constructor(
    val googleNewsApi: GoogleNewsApi,
    val googleNewsDatabase: GoogleNewsDatabase,
    val disposable: CompositeDisposable
): PageKeyedDataSource<String, ArticlesEntity>() {

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()
    val articlesObservable: ReplaySubject<ArticlesEntity> = ReplaySubject.create()

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, ArticlesEntity>) {
        networkState.postValue(NetworkState.Loading)
        disposable.add(
            googleNewsApi.getTopHeadlines(
                BuildConfig.GOOGLE_NEWS_API_KEY,
                "en",
                "21",
                "1"
            )
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        callback.onResult(response.body()!!.articles!!, "1", "2")
                        response.body()!!.articles!!.forEach(articlesObservable::onNext)
                        networkState.postValue(NetworkState.Finished)
                    } else networkState.postValue(NetworkState.Error(response.message()))

                }, { throwable ->
                    networkState.postValue(NetworkState.Error(throwable.localizedMessage?:"Unknown Error"))
                    callback.onResult(emptyList(), "1", "2")
                })
        )
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, ArticlesEntity>) {
        val nextPage = (params.key.toInt() + 1).toString()
        networkState.postValue(NetworkState.Loading)
        disposable.add(
            googleNewsApi.getTopHeadlines(
                BuildConfig.GOOGLE_NEWS_API_KEY,
                "en",
                "21",
                "1"
            )
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (response.isSuccessful) {
                        callback.onResult(response.body()!!.articles!!, nextPage)
                        response.body()!!.articles!!.forEach(articlesObservable::onNext)
                        networkState.postValue(NetworkState.Finished)
                    } else networkState.postValue(NetworkState.Error(response.message()))

                }, { throwable ->
                    networkState.postValue(NetworkState.Error(throwable.localizedMessage ?: "Unknown Error"))
                    callback.onResult(emptyList(), nextPage)
                })

        )
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, ArticlesEntity>) {
    }

}
