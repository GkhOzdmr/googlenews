package bonial.googlenews.android.features.topheadlines.presentation.fragment


import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import bonial.googlenews.android.R
import bonial.googlenews.android.core.extension.viewModel
import bonial.googlenews.android.core.platform.BaseFragment
import bonial.googlenews.android.features.topheadlines.domain.model.ArticleModel
import bonial.googlenews.android.features.topheadlines.domain.usecase.CalculateWhenPosted
import bonial.googlenews.android.features.topheadlines.domain.usecase.CalculatedTimes
import bonial.googlenews.android.features.topheadlines.presentation.viewmodel.TopHeadlinesViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_article_detail.*
import timber.log.Timber
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

/**
 * This constant is used to post/receive [ArticleModel] within bundle
 */
private const val ARG_ARTICLE_MODEL = "ARG_ARTICLE_MODEL"

/**
 * Used to play transition animations between fragments
 */
private const val ARG_TRANSITION_NAME = "ARG_TRANSITION_NAME"

/**
 * A simple [Fragment] subclass.
 *
 * This fragment is used for displaying the details of the selected article.
 */
class ArticleDetailFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_article_detail

    override fun onBackPressed(): Boolean {
        showActionBar()
        return true
    }

    private var articleModel: ArticleModel? = null
    private var transitionName: String? = ""
    lateinit var topHeadlinesViewModel: TopHeadlinesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTransition()
        setArgs()
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    /**
     * Observes ViewModel observables
     */
    private fun initViewModel() {
        topHeadlinesViewModel = viewModel(viewModelFactory) { }
    }

    /**
     * Initializes view components
     */
    private fun initView() {
        hideActionBar()
        setContent()
        setListeners()

        img_article.transitionName = transitionName
        loadImage()
    }

    /**
     * Sets listeners for various widgets and components
     */
    private fun setListeners() {
        btn_article_source_link.setOnClickListener {
            openArticleSource()
        }
    }

    /**
     * Sets the arguments to fields that returned from creation of this fragment.
     */
    private fun setArgs() {
        //Sets the arguments
        arguments?.let {
            if (arguments!!.getParcelable<ArticleModel>(ARG_ARTICLE_MODEL) != null) {
                articleModel = arguments!!.getParcelable<ArticleModel>(ARG_ARTICLE_MODEL)
            }

            if (arguments!!.getString(ARG_TRANSITION_NAME) != null) {
                transitionName = arguments!!.getString(ARG_TRANSITION_NAME)
            }
        }
    }

    /**
     * Sets the transition animations from the RecyclerView item to this fragment
     */
    private fun setTransition() {
        //Postpones creation until the image is loaded
        postponeEnterTransition()

        //Setting the transition animations
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.transition_change_bounds)
    }

    /**
     * Loads the image of the article.
     * After load, fragment starts.
     * In order to play transition animation on image view properly
     * we have to first get the image.
     * After image loaded, fragment can continue its own lifecycle as usual.
     */
    private fun loadImage() {
        if (articleModel != null) {
            //Loads image
            Glide.with(context!!)
                .load(Uri.parse(articleModel?.urlToImage?:""))
                .error(ContextCompat.getDrawable(context!!, R.drawable.placeholder_no_image))
                .placeholder(ContextCompat.getDrawable(context!!, R.drawable.placeholder_no_image))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        startPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        startPostponedEnterTransition()
                        return false
                    }
                })
                .into(img_article)
        }
    }

    /**
     * Combines data with view objects
     */
    private fun setContent() {
        if (articleModel != null) {
            tv_article_title.text = articleModel!!.title?:""
            tv_article_source.text = articleModel!!.source?.name?:""

            val publishDate = articleModel!!.publishedAt!!
            disposable.add(
                topHeadlinesViewModel.getCalculatedPostDate(publishDate)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        tv_article_publish_time.text = getPostDayString(it)
                        Timber.d("yearCount ${it.yearCount} monthCount ${it.monthCount} dayCount ${it.dayCount} hourCount ${it.hourCount} minuteCount ${it.minuteCount}")
                    },{t ->
                        Timber.d("setContent.error.localizedMessage: ${t.localizedMessage}")
                        tv_article_publish_time.visibility = View.GONE
                    })
            )

            tv_article_description.text = articleModel!!.description?:""
        }
    }

    private fun getPostDayString(calculatedTimes: CalculatedTimes): String {
        return when {
            calculatedTimes.yearCount!! > 0 -> {
                resources.getQuantityString(R.plurals.post_date_calculation_year, calculatedTimes.yearCount, calculatedTimes.yearCount)
            }

            calculatedTimes.monthCount!! > 0 -> {
                resources.getQuantityString(R.plurals.post_date_calculation_month, calculatedTimes.monthCount, calculatedTimes.monthCount)
            }

            calculatedTimes.dayCount!! > 0 -> {
                resources.getQuantityString(R.plurals.post_date_calculation_day, calculatedTimes.dayCount, calculatedTimes.dayCount)
            }

            calculatedTimes.hourCount!! > 0 -> {
                resources.getQuantityString(R.plurals.post_date_calculation_hour, calculatedTimes.hourCount, calculatedTimes.hourCount)
            }

            calculatedTimes.minuteCount!! > 0 -> {
                resources.getQuantityString(R.plurals.post_date_calculation_minute, calculatedTimes.minuteCount, calculatedTimes.minuteCount)
            }

            else -> { arguments!!.getString(R.string.post_date_calculation_moment.toString()) }
        }
    }

    /**
     * Redirects user to a web view which displays the source of the article
     */
    private fun openArticleSource() {
        //If no http exists within link, adds, else uses directly
        val url =
            if (!articleModel?.url?.startsWith("http://")!! && !articleModel?.url?.startsWith("https://")!!) {
                "http://" + articleModel?.url
            } else articleModel?.url


        //Intent for browsing
        val browserIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )

        startActivity(browserIntent)
    }

    companion object {

        /**
         * Returns instance of this fragment class with bundle
         *
         * @param articleModel [ArticleModel] to put in arguments of the fragment
         * @param transitionName For transition animation
         */
        @JvmStatic
        fun newInstance(articleModel: ArticleModel, transitionName: String): ArticleDetailFragment {

            return ArticleDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ARTICLE_MODEL, articleModel)
                    putString(ARG_TRANSITION_NAME, transitionName)
                }
            }
        }
    }

}
