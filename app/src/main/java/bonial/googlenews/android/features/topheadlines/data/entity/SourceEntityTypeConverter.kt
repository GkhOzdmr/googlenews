package bonial.googlenews.android.features.topheadlines.data.entity

import androidx.room.TypeConverter
import com.google.gson.Gson

/**
 * Converter for ROOM to convert DTO class within Entity.
 */
class SourceEntityTypeConverter {
    @TypeConverter
    fun toSourceEntity(json: String?): SourceEntity? {
        return Gson().fromJson(json, SourceEntity::class.java)?:null
    }

    @TypeConverter
    fun toJson(sourceEntity: SourceEntity?): String? {
        return Gson().toJson(sourceEntity, SourceEntity::class.java)?:null
    }
}