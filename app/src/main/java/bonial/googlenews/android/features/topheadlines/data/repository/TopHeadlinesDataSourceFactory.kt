package bonial.googlenews.android.features.topheadlines.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.core.service.GoogleNewsApi
import bonial.googlenews.android.core.service.GoogleNewsDatabase
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.ReplaySubject

/**
 * Data source factory for Android Paging Library.
 * Please refer to Paging Library for detailed information.(link:https://developer.android.com/topic/libraries/architecture/paging)
 */
class TopHeadlinesDataSourceFactory constructor(
    val googleNewsApi: GoogleNewsApi,
    val googleNewsDatabase: GoogleNewsDatabase,
    val disposable: CompositeDisposable
) : DataSource.Factory<String, ArticlesEntity>() {

    var networkState: MutableLiveData<NetworkState>
    var articles: ReplaySubject<ArticlesEntity>
    var dataSource: TopHeadlinesDataSource

    init {
        dataSource = TopHeadlinesDataSource(googleNewsApi, googleNewsDatabase, disposable)
        networkState = dataSource.networkState
        articles = dataSource.articlesObservable
    }

    override fun create(): DataSource<String, ArticlesEntity> {
        return dataSource
    }

}