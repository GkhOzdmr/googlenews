package bonial.googlenews.android.features.topheadlines.domain.model

//TODO Document
data class TopHeadlinesModel(
    val status : String?,
    val totalResults : Int?,
    val articles : List<ArticleModel>?
)