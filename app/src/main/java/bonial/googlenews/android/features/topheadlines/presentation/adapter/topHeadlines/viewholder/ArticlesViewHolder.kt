package bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import bonial.googlenews.android.R
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.presentation.adapter.topHeadlines.TopHeadlinesAdapter
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.item_top_headlines.view.*

/**
 * View Holder class for displaying [ArticlesEntity] data
 */
class ArticlesViewHolder(
    view: View,
    private val notifier: TopHeadlinesAdapter.Notifier,
    private val glide: RequestManager
) : RecyclerView.ViewHolder(view){

    val cl_item_top_headlines = view.cl_item_top_headlines
    val img_headline = view.img_headline
    val tv_article_title = view.tv_article_title
    val tv_article_source = view.tv_article_source
    val tv_article_description = view.tv_article_description
    val btn_go_to_article = view.btn_go_to_article

    fun bind(article: ArticlesEntity?) {
        if(article == null) return
        glide.load(article.urlToImage)
            .error(R.drawable.placeholder_no_image)
            .placeholder(R.drawable.placeholder_no_image)
            .into(img_headline)

            tv_article_title.text = article.title ?: ""
            tv_article_source.text = article.source?.name ?: ""
            tv_article_description.text = article.description ?: ""

            ViewCompat.setTransitionName(img_headline, article.title)

        btn_go_to_article.setOnClickListener {
            notifier.onHeadlineClick(article.toModel(), img_headline)
        }
    }

    companion object {
        fun create(
            parent: ViewGroup,
            notifier: TopHeadlinesAdapter.Notifier,
            glide: RequestManager
        ): ArticlesViewHolder {
            return ArticlesViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_top_headlines, parent, false),
                notifier,
                glide
            )
        }
    }

}