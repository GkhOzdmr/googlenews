package bonial.googlenews.android.features.topheadlines.domain.usecase

/**
 * This particular DTO is used to restore the amount of
 * specific date field' count starting from today to back to published date of the article
 *
 * The field that's used to get publish date is belongs to [ArticleModel]'s "publishDate" field.
 *
 * These field are to be used to return strings such as "1 day ago" etc.
 */
data class CalculatedTimes(
    val yearCount: Int?,
    val monthCount: Int?,
    val dayCount: Int?,
    val hourCount: Int?,
    val minuteCount: Int?
)