package bonial.googlenews.android.features.topheadlines.presentation.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import bonial.googlenews.android.core.functional.NetworkState
import bonial.googlenews.android.core.platform.BaseViewModel
import bonial.googlenews.android.features.topheadlines.data.entity.ArticlesEntity
import bonial.googlenews.android.features.topheadlines.domain.usecase.CalculateWhenPosted
import bonial.googlenews.android.features.topheadlines.domain.usecase.CalculatedTimes
import bonial.googlenews.android.features.topheadlines.domain.usecase.GetArticles
import com.google.gson.internal.bind.util.ISO8601Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import javax.inject.Inject

/**
 * This ViewModel implementation is for handling business from data to domain/presentation layer for
 * fetching and displaying [TopHeadlinesEntity] from Google News API
 */
class TopHeadlinesViewModel @Inject constructor(
    val getArticles: GetArticles,
    val calculateWhenPosted: CalculateWhenPosted
) : BaseViewModel() {

    var networkState: MutableLiveData<NetworkState>
    var articlesObserver: MediatorLiveData<PagedList<ArticlesEntity>>

    init {
        networkState = getArticles.networkState
        articlesObserver = getArticles.articlesObservable
    }

    override fun onCleared() {
        super.onCleared()
        getArticles.disposeSubscriptions()
    }

    fun getCalculatedPostDate(publishDate: String): Observable<CalculatedTimes> {
        return calculateWhenPosted.execute(CalculateWhenPosted.Params(publishDate, getTodayInIso()))!!
    }

    private fun getTodayInIso(): String {
        val currentTimeInMillis = System.currentTimeMillis()
        val date = Date(currentTimeInMillis)
        val dateFormat = ISO8601Utils.format(date)
        Timber.d("dateFormat ${dateFormat.toString()}")
        return dateFormat
    }

}