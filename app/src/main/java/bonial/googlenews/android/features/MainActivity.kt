package bonial.googlenews.android.features

import android.os.Bundle
import androidx.fragment.app.Fragment
import bonial.googlenews.android.R
import bonial.googlenews.android.core.platform.BaseFragment
import bonial.googlenews.android.features.topheadlines.presentation.fragment.TopHeadlinesFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.lang.IndexOutOfBoundsException
import android.content.Intent

/**
 * This application is designed with the "Single Activity" principle in mind. Thus this particular
 * activity used as base activity to handle fragment navigations etc.
 *
 * All the business should be handled with via Fragments.
 */
class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navToStart()
    }

    /**
     * If fragment implementation of onBackPress allows, calls super.onBackPressed()
     */
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) finishApp()
        val currentFragment = getCurrentFragment()
        if (currentFragment is BaseFragment) {
            Timber.d("currentFragment is BaseFragment")
            if (currentFragment.onBackPressed()) super.onBackPressed()
        } else {
            Timber.d("else")
            super.onBackPressed()
        }
    }

    /**
     * Closes the application
     */
    private fun finishApp() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    /**
     * Gets the current fragment from back stack
     */
    private fun getCurrentFragment(): Fragment? {
        val fm = supportFragmentManager
        val lastIndex = fm.backStackEntryCount - 1
        val lastFragment = fm.getBackStackEntryAt(lastIndex)
        return fm.findFragmentByTag(lastFragment.name)
    }

    private fun navToStart() {
        val beginnerFragment = TopHeadlinesFragment.newInstance()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.host_frame, beginnerFragment, beginnerFragment::class.simpleName)
            addToBackStack(beginnerFragment::class.simpleName)
            commit()
        }
    }

}