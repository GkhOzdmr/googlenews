plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "bonial.googlenews.android"
        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    defaultConfig {
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
    }

    buildTypes {

        getByName("debug") {
            isMinifyEnabled = false
            //TODO Carry fields to prod
            buildConfigField("String", "GOOGLE_NEWS_BASE_URL", "\"https://newsapi.org/v2/\"")
            buildConfigField("String", "GOOGLE_NEWS_API_KEY", "\"141b2c0b51c04645b35c4bcfbf21493c\"")
        }

        getByName("release") {
            isMinifyEnabled = true
            buildConfigField("String", "GOOGLE_NEWS_BASE_URL", "\"https://newsapi.org/v2/\"")
            buildConfigField("String", "GOOGLE_NEWS_API_KEY", "\"141b2c0b51c04645b35c4bcfbf21493c\"")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

var jet_annotation_ver = "16.0.1"
var kotlin_version = "1.3.41"

var timber_version = "4.7.1"
var dagger_version = "2.22.1"
var retrofit_version = "2.6.1"
var okhttp_version = "3.14.2"
var okio_version = "2.2.2"

var rxjava_version = "2.2.9"
var rxandroid_version = "2.1.1"
var rxbindings_version = "3.0.0-alpha2"

var lifecycle_version = "2.0.0"
var navigation_version = "2.1.0-alpha01"
var room_version = "2.2.0-alpha02"
var paging_version = "2.1.0"

var eventbus_version = "3.1.1"

var leak_canary_version = "2.0-alpha-2"
var leak_canary_noop_version = "1.6.3"
var mockk_version = "1.9.3"
var glide_version = "4.9.0"

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    compileOnly("org.jetbrains:annotations:$jet_annotation_ver")
    testCompileOnly("org.jetbrains:annotations:$jet_annotation_ver")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")

    //ANDROID
    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.core:core-ktx:1.0.2")
    implementation("com.android.support.constraint:constraint-layout:1.1.3")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("com.google.android.material:material:1.0.0")
    implementation("com.android.support:multidex:1.0.3")

    //ANDROID ARCHITECTURE COMP.
    //ROOM
    implementation("androidx.room:room-runtime:$room_version")
    implementation("androidx.room:room-rxjava2:$room_version")
    implementation("androidx.room:room-ktx:$room_version")
    kapt("androidx.room:room-compiler:$room_version")
    //PAGING
    implementation("androidx.paging:paging-runtime:$paging_version")
    implementation("androidx.paging:paging-rxjava2:$paging_version")
    //ViewModel
    implementation("android.arch.lifecycle:extensions:1.1.1")
    implementation("android.arch.lifecycle:viewmodel:1.1.1")
    //Lifecycle
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version")
    implementation("androidx.lifecycle:lifecycle-livedata:$lifecycle_version")
    kapt("androidx.lifecycle:lifecycle-compiler:$lifecycle_version")
    //This can be deleted if app doesn't use lifecycle annotations see: (https://stackoverflow.com/a/49602453)

    //RX JAVA
    implementation("io.reactivex.rxjava2:rxjava:$rxjava_version")
    //RX ANDROID
    implementation("io.reactivex.rxjava2:rxandroid:$rxandroid_version")

    //DAGGER 2
    implementation("com.google.dagger:dagger:$dagger_version")
    implementation("com.google.dagger:dagger-android:$dagger_version")
    kapt("com.google.dagger:dagger-compiler:$dagger_version")
    implementation("com.google.dagger:dagger-android-support:$dagger_version")
    kapt("com.google.dagger:dagger-android-processor:$dagger_version")

    //RETROFIT
    implementation("com.squareup.retrofit2:retrofit:$retrofit_version")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofit_version")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit_version")

    //OKHTTP 3
    implementation("com.squareup.okhttp3:okhttp:$okhttp_version")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttp_version")

    //OKIO
    implementation("com.squareup.okio:okio:$okio_version")

    //TIMBER
    implementation("com.jakewharton.timber:timber:${timber_version}")

    //TESTING
    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test:runner:1.2.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
    testImplementation("org.robolectric:robolectric:4.3")
    testImplementation("androidx.test:core:1.2.0")
    testImplementation("io.mockk:mockk:$mockk_version")

    //LEAK CANARY
    debugImplementation("com.squareup.leakcanary:leakcanary-android:$leak_canary_version")
    releaseImplementation("com.squareup.leakcanary:leakcanary-android-no-op:$leak_canary_noop_version")
    testImplementation("com.squareup.leakcanary:leakcanary-android-no-op:$leak_canary_noop_version")

    //GLIDE
    implementation("com.github.bumptech.glide:glide:$glide_version")
    kapt("com.github.bumptech.glide:compiler:$glide_version")

    //FACEBOOK SHIMMER
    implementation("com.facebook.shimmer:shimmer:0.4.0")
}
